# librist-sys

[![crates.io version](https://img.shields.io/crates/v/librist-sys.svg)](https://crates.io/crates/librist-sys)
[![Documentation](https://docs.rs/librist-sys/badge.svg)](https://docs.rs/librist-sys)

Bindgen-based wrapper for librist.

See [librist-rust](../librist-rust) for idiomatic Rust bindings based on this crate.

`librist` is included as a git submodule at a specific commit-id.  This means that updates to the librist API can't
break this crate.  On the other hand it also means that that even non-API/ABI-effecting bugfixes to librist will not
become available via this crate unless the git submodule commit-id is updated and a new release of librist-sys is
made. 

This crate currently builds and links to a static librist.

```shell
apt install musl-tools meson libclang1 llvm clang

CC=musl-gcc cargo +1.50 test --target=x86_64-unknown-linux-musl
```
